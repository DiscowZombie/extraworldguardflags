package fr.discowzombie.extraworldguardflags;

import com.sk89q.worldedit.util.Location;
import com.sk89q.worldguard.LocalPlayer;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.session.MoveType;
import com.sk89q.worldguard.session.Session;
import com.sk89q.worldguard.session.handler.FlagValueChangeHandler;
import com.sk89q.worldguard.session.handler.Handler;
import org.bukkit.Bukkit;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public final class FlagsHandler extends FlagValueChangeHandler<StateFlag.State> {

    public static final Factory FACTORY = new Factory();
    private static final long MESSAGE_THRESHOLD = 1000 * 2;
    private final Map<UUID, Long> nextMessageTime = new HashMap<>();

    public FlagsHandler(Session session) {
        super(session, Plugin.TELEPORT_ON_GROUND_FIREWORK);
    }

    @Override
    protected void onInitialValue(LocalPlayer player, ApplicableRegionSet set, StateFlag.State value) {
    }

    @Override
    protected boolean onSetValue(LocalPlayer player, Location from, Location to, ApplicableRegionSet toSet, StateFlag.State currentValue, StateFlag.State lastValue, MoveType moveType) {
        final var bukkitPlayer = Bukkit.getPlayer(player.getUniqueId());
        if (bukkitPlayer != null && moveType == MoveType.GLIDE) {
            final var highestBlock = bukkitPlayer.getLocation().getWorld().getHighestBlockAt(to.getBlockX(), to.getBlockZ());
            final var location = highestBlock.getLocation();
            location.add(0, 1, 0);
            location.setYaw(bukkitPlayer.getLocation().getYaw());
            location.setPitch(bukkitPlayer.getLocation().getPitch());
            bukkitPlayer.teleport(location);

            final Long nextMessageTime = this.nextMessageTime.getOrDefault(player.getUniqueId(), 0L);
            if (nextMessageTime == null || nextMessageTime <= System.currentTimeMillis()) {
                final var pl = Bukkit.getPlayer(player.getUniqueId());
                if (pl != null) {
                    player.printRaw(toSet.queryValue(player, Plugin.TELEPORT_ON_GROUND_FIREWORK_MESSAGE));
                }

                this.nextMessageTime.put(player.getUniqueId(), System.currentTimeMillis() + MESSAGE_THRESHOLD);
            }
        }

        return true;
    }

    @Override
    protected boolean onAbsentValue(LocalPlayer player, Location from, Location to, ApplicableRegionSet toSet, StateFlag.State lastValue, MoveType moveType) {
        return true;
    }

    @Override
    public boolean onCrossBoundary(LocalPlayer player, Location from, Location to, ApplicableRegionSet toSet, Set<ProtectedRegion> entered, Set<ProtectedRegion> exited, MoveType moveType) {
        return super.onCrossBoundary(player, from, to, toSet, entered, exited, moveType);
    }

    public static class Factory extends Handler.Factory<FlagsHandler> {

        @Override
        public FlagsHandler create(Session session) {
            return new FlagsHandler(session);
        }

    }

}

