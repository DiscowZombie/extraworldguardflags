package fr.discowzombie.extraworldguardflags;

import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.flags.StringFlag;
import com.sk89q.worldguard.protection.flags.registry.FlagConflictException;
import fr.discowzombie.extraworldguardflags.listeners.EnderChestOpenListener;
import fr.discowzombie.extraworldguardflags.listeners.EntitySpawnListener;
import fr.discowzombie.extraworldguardflags.listeners.FireworksUseListener;
import fr.discowzombie.extraworldguardflags.listeners.NetherPortalListener;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Level;

public final class Plugin extends JavaPlugin {

    public static StateFlag PREVENT_OPENING_ENDERCHEST;
    public static StateFlag PREVENT_OPENING_SMITHING_TABLE;
    public static StateFlag PREVENT_OPENING_GRINDSTONE;
    public static StateFlag TELEPORT_ON_GROUND_FIREWORK;
    /**
     * @deprecated The type of this flag will change from a StringFlag to a ComponentFlag to support JSON text
     * in a future release. If you depend on the type of this flag, take proper precaution for future breakage.
     */
    @Deprecated
    public static StringFlag TELEPORT_ON_GROUND_FIREWORK_MESSAGE;
    /**
     * @deprecated The type of this flag will change from a StringFlag to a ComponentFlag to support JSON text
     * in a future release. If you depend on the type of this flag, take proper precaution for future breakage.
     */
    @Deprecated
    public static StringFlag PREVENT_FIREWORKS_MESSAGE;
    public static StateFlag NATURAL_SPAWNING;
    public static StateFlag PREVENT_NETHER_PORTAL;

    @Override
    public void onLoad() {
        final var registry = WorldGuard.getInstance().getFlagRegistry();

        try {
            final var preventOpeningEnderchest = new StateFlag("prevent-opening-enderchest", false);
            registry.register(preventOpeningEnderchest);
            PREVENT_OPENING_ENDERCHEST = preventOpeningEnderchest;

            final var preventOpeningSmithingTable = new StateFlag("prevent-opening-smithing", false);
            registry.register(preventOpeningSmithingTable);
            PREVENT_OPENING_SMITHING_TABLE = preventOpeningSmithingTable;

            final var preventOpeningGrindstone = new StateFlag("prevent-opening-grindstone", false);
            registry.register(preventOpeningGrindstone);
            PREVENT_OPENING_GRINDSTONE = preventOpeningGrindstone;

            final var teleportOnGroundFirework = new StateFlag("teleport-on-ground-firework", false);
            registry.register(teleportOnGroundFirework);
            TELEPORT_ON_GROUND_FIREWORK = teleportOnGroundFirework;

            final var preventFireworksMessage = new StringFlag("prevent-fireworks-message",
                    LegacyComponentSerializer.legacyAmpersand().serialize(
                            Component.text("You are not permitted to use fireworks in this area.", NamedTextColor.RED)));
            registry.register(preventFireworksMessage);
            PREVENT_FIREWORKS_MESSAGE = preventFireworksMessage;

            final var teleportOnGroundFireworkMessage = new StringFlag("teleport-on-ground-firework-message",
                    LegacyComponentSerializer.legacyAmpersand().serialize(
                            Component.text("You are not permitted to fly in this area.", NamedTextColor.RED)));
            registry.register(teleportOnGroundFireworkMessage);
            TELEPORT_ON_GROUND_FIREWORK_MESSAGE = teleportOnGroundFireworkMessage;

            final var naturalSpawning = new StateFlag("natural-spawning", true);
            registry.register(naturalSpawning);
            NATURAL_SPAWNING = naturalSpawning;

            final var preventNetherPortal = new StateFlag("prevent-nether-portal", false);
            registry.register(preventNetherPortal);
            PREVENT_NETHER_PORTAL = preventNetherPortal;
        } catch (FlagConflictException e) {
            Bukkit.getLogger().log(Level.SEVERE, "Unable to register custom WorldGuard flag", e);
            Bukkit.getPluginManager().disablePlugin(this);
        }
    }

    @Override
    public void onEnable() {
        final var pluginManager = Bukkit.getPluginManager();
        pluginManager.registerEvents(new EnderChestOpenListener(), this);
        pluginManager.registerEvents(new EntitySpawnListener(), this);
        pluginManager.registerEvents(new FireworksUseListener(), this);
        pluginManager.registerEvents(new NetherPortalListener(), this);

        final var sessionManager = WorldGuard.getInstance().getPlatform().getSessionManager();
        sessionManager.registerHandler(FlagsHandler.FACTORY, null);
    }

}
