package fr.discowzombie.extraworldguardflags.listeners;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import fr.discowzombie.extraworldguardflags.Plugin;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;

import java.util.EnumSet;
import java.util.Set;

public final class EntitySpawnListener implements Listener {

    private static final Set<CreatureSpawnEvent.SpawnReason> ALLOWED_REASONS = EnumSet.of(
            CreatureSpawnEvent.SpawnReason.SPAWNER_EGG,
            CreatureSpawnEvent.SpawnReason.COMMAND,
            CreatureSpawnEvent.SpawnReason.CUSTOM);

    @EventHandler
    private void onPlayerInteractEnderChest(CreatureSpawnEvent event) {
        final var reason = event.getSpawnReason();

        if (ALLOWED_REASONS.contains(reason) || event.getEntity() instanceof Player)
            return; // Does not cancel the event

        final var container = WorldGuard.getInstance().getPlatform().getRegionContainer();
        final var query = container.createQuery();
        final var set = query.getApplicableRegions(BukkitAdapter.adapt(event.getLocation()));

        if (!set.testState(null, Plugin.NATURAL_SPAWNING)) {
            event.setCancelled(true);
        }
    }

}
