package fr.discowzombie.extraworldguardflags.listeners;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import fr.discowzombie.extraworldguardflags.Plugin;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

public final class EnderChestOpenListener implements Listener {

    @EventHandler
    private void onPlayerInteractEnderChest(PlayerInteractEvent event) {
        final var clickedBlock = event.getClickedBlock();
        final var player = event.getPlayer();

        // We only care for ender chest
        if (clickedBlock != null &&
                event.getAction().isRightClick() &&
                clickedBlock.getType() == Material.ENDER_CHEST) {
            final var container = WorldGuard.getInstance().getPlatform().getRegionContainer();
            final var query = container.createQuery();
            final var set = query.getApplicableRegions(BukkitAdapter.adapt(player.getLocation()));

            final var localPlayer = WorldGuardPlugin.inst().wrapPlayer(player);

            // Opening ender chest is cancelled
            if (set.testState(localPlayer, Plugin.PREVENT_OPENING_ENDERCHEST)) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    private void onPlayerInteractSmithing(PlayerInteractEvent event) {
        final var clickedBlock = event.getClickedBlock();
        final var player = event.getPlayer();

        // We only care for ender chest
        if (clickedBlock != null &&
                event.getAction().isRightClick() &&
                clickedBlock.getType() == Material.SMITHING_TABLE) {
            final var container = WorldGuard.getInstance().getPlatform().getRegionContainer();
            final var query = container.createQuery();
            final var set = query.getApplicableRegions(BukkitAdapter.adapt(player.getLocation()));

            final var localPlayer = WorldGuardPlugin.inst().wrapPlayer(player);

            if (set.testState(localPlayer, Plugin.PREVENT_OPENING_SMITHING_TABLE)) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    private void onPlayerInteractGrindstone(PlayerInteractEvent event) {
        final var clickedBlock = event.getClickedBlock();
        final var player = event.getPlayer();

        // We only care for ender chest
        if (clickedBlock != null &&
                event.getAction().isRightClick() &&
                clickedBlock.getType() == Material.GRINDSTONE) {
            final var container = WorldGuard.getInstance().getPlatform().getRegionContainer();
            final var query = container.createQuery();
            final var set = query.getApplicableRegions(BukkitAdapter.adapt(player.getLocation()));

            final var localPlayer = WorldGuardPlugin.inst().wrapPlayer(player);

            if (set.testState(localPlayer, Plugin.PREVENT_OPENING_GRINDSTONE)) {
                event.setCancelled(true);
            }
        }
    }

}
