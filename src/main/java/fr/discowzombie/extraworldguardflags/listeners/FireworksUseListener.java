package fr.discowzombie.extraworldguardflags.listeners;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.WorldGuard;
import fr.discowzombie.extraworldguardflags.Plugin;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.logging.Level;

public final class FireworksUseListener implements Listener {

    @EventHandler
    private void onUseFireworks(PlayerInteractEvent event) {
        if (event.getItem() != null && event.getItem().getType() == Material.FIREWORK_ROCKET) {
            final var container = WorldGuard.getInstance().getPlatform().getRegionContainer();
            final var location = event.getPlayer().getLocation();
            final var regionManager = container.get(BukkitAdapter.adapt(location.getWorld()));

            if (regionManager == null) {
                Plugin.getPlugin(Plugin.class).getLogger().log(Level.WARNING, "Unable to retrieve RegionManager for world " +
                        location.getWorld().getName());
                return;
            }

            final var applicableSet = regionManager.getApplicableRegions(
                    BlockVector3.at(location.getBlockX(), location.getBlockY(), location.getBlockZ()));
            if (applicableSet.testState(null, Plugin.TELEPORT_ON_GROUND_FIREWORK)) {
                event.setCancelled(true);

                final var message = LegacyComponentSerializer.legacyAmpersand()
                        .deserializeOrNull(applicableSet.queryValue(null, Plugin.PREVENT_FIREWORKS_MESSAGE));
                if (message != null) {
                    event.getPlayer().sendMessage(message);
                }
            }
        }
    }

}
