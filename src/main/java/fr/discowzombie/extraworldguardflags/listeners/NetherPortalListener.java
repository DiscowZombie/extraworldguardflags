package fr.discowzombie.extraworldguardflags.listeners;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.WorldGuard;
import fr.discowzombie.extraworldguardflags.Plugin;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.PortalCreateEvent;

import java.util.logging.Level;

public final class NetherPortalListener implements Listener {

    @EventHandler
    private void onPlayerPortalCreate(PortalCreateEvent event) {
        if (event.getReason() == PortalCreateEvent.CreateReason.END_PLATFORM)
            // Do nothing for end
            return;

        final var blocks = event.getBlocks();
        if (blocks.isEmpty()) {
            Plugin.getPlugin(Plugin.class).getLogger().log(Level.WARNING, "No block for portal at " + event.getWorld().getName());
            return;
        }

        final var container = WorldGuard.getInstance().getPlatform().getRegionContainer();
        final var regionManager = container.get(BukkitAdapter.adapt(blocks.get(0).getWorld()));

        if (regionManager == null) {
            Plugin.getPlugin(Plugin.class).getLogger().log(Level.WARNING, "Unable to retrieve RegionManager for world " +
                    blocks.get(0).getWorld().getName());
            return;
        }

        final var applicableSet = regionManager.getApplicableRegions(
                BlockVector3.at(blocks.get(0).getLocation().getBlockX(), blocks.get(0).getLocation().getBlockY(), blocks.get(0).getLocation().getBlockZ()));
        if (applicableSet.testState(null, Plugin.PREVENT_NETHER_PORTAL)) {
            event.setCancelled(true);

            if (event.getEntity() instanceof final Player player) {
                player.sendMessage(Component.text("Vous ne pouvez pas créer de portail ici.", NamedTextColor.RED));
            }
        }
    }

}
